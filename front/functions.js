/** 
* Number of practice series in both practice parts of test
* @global
* @param {String} practiceSeries     
*/
var practiceSeries = 3;
/** 
* Number of real test series in both tests
* @global
* @param {String} testSeries     
*/
var testSeries = 60;

/** 
* Function invoked after page is loaded for the first time.
* Hides unnecesarry elements.
* @global
* @method anonymous-function
*/
$(document).ready(function(){
    $("#symbolBox").hide();
    $("#goToFirstPractice").hide();
    $("#goToFirstTest").hide();
    $("#goToSecondPractice").hide();
    $("#goToSecondTest").hide();
    $("#firstPracticeInstruction").hide();
    $("#firstTestInstruction").hide();
    $("#secondPracticeInstruction").hide();
    $("#secondTestInstruction").hide();
    $("#finishMessage").hide();
});

/** 
* Function invoked after user read first instruction and clicked on button.
* Hides unnecesarry elements.
* Starts pracitce series.
* @global
* @method anonymous-function
*/
$(document).ready(function() {
    $("#goToFirstPractice").click(function() {
        $("#firstPracticeInstruction").hide();
        $(this).hide();
        $("#symbolBox").show();
        symbols = getSymbolsSeries(practiceSeries, 1).slice();
        doSternbergTest(updateSymbolBox, symbols.length, "firstPractice");
    });
})

/** 
* Function invoked after user clicked on button, redirects to first part of test.
* Hides unnecesarry elements.
* @global
* @method anonymous-function
*/
$(document).ready(function(){
    $("#goToFirstTest").click(function(){
        $("#firstTestInstruction").hide();
        $(this).hide();
        $("#symbolBox").show();
        counter = 0;
        symbols = getSymbolsSeries(testSeries, 1).slice();
        doSternbergTest(updateSymbolBox, symbols.length, "firstTest");
    });
});

/** 
* Function invoked after user clicked on button.
* Hides unnecesarry elements.
* Starts second pracitce series.
* @global
* @method anonymous-function
*/
$(document).ready(function(){
    $("#goToSecondPractice").click(function(){
        $("#secondPracticeInstruction").hide();
        $(this).hide();
        $("#symbolBox").show();
        counter = 0;
        symbols = getSymbolsSeries(practiceSeries, 2).slice();
        doSternbergTest(updateSymbolBox, symbols.length, "secondPractice");
    });
});

/** 
* Function invoked after user clicked on button, redirects to second part of test.
* Hides unnecesarry elements.
* @global
* @method anonymous-function 
*/
$(document).ready(function(){
    $("#goToSecondTest").click(function(){
        $("#secondTestInstruction").hide();
        $(this).hide();
        $("#symbolBox").show();
        counter = 0;
        symbols = getSymbolsSeries(testSeries, 2).slice();
        doSternbergTest(updateSymbolBox, symbols.length, "secondTest");
    });
});
