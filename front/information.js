/** 
* Chosen sex by user.
* @global
* @param {String} chosenSex     
*/
var chosenSex 
/** 
* Chosen index number by user.
* @global
* @param {Number} chosenIndex    
*/
var chosenIndex

 /**
 * Function invoked after click on button. 
 * Sets variables chosenSex and chosenIndex to chosen values.
 * If values from fields are not apropriate, shows alert window.
 * In other case, redirects to next test step - first instruction.
 * @method anonymous-function
 * @global
 */
$(document).ready(function(){
    $("#submit-info").click(function() {
    	if($('input[name=gender]:checked', '#sex').val()==undefined)
    		alert("Zaznacz płeć.");
        else if($('#index').val().length != 6)
            alert("Wpisz poprawny numer indeksu.")
    	else {
            chosenIndex = $('#index').val()
            chosenSex = $('input[name=gender]:checked', '#sex').val()
            $(".info-container").hide();
            $("#goToFirstPractice").show();
            $("#firstPracticeInstruction").show();
    	}
	});
})
