/** 
* Symbols showed in every series.
* @global
* @param {array} practiceSeries     
*/
const constSymbols = ["A", "B", "C", "D", "E", "F",
                "G", "H", "I", "J", "K", "L",
                "M", "N", "O", "Q", "P", "R",
                "S", "T", "U", "W", "Y", "Z",
                "V", "0", "1", "2", "3", "4",
                "5", "6", "7", "8", "9"];

/** 
* Positions of every symbol in sequence. 
* @global
* @param {Array} positions    
*/
var positions = [];
/** 
* Number of symbols in each sequence. 
* @global
* @param {array} testSymbolsNumber
*/
var testSymbolsNumber = 8;

/** 
* Creates single symbol sequence.
* @global
* @method getSumbolSeries
* @param {array} numberOfSingleSeries number of series in each test
* @param {array} checkSymbols symbols that are concated in each iteration to sequence
* @return {array} sequence all symbols from all series in test
*/
function getSymbolsSeries(numberOfSingleSeries, checkSymbols){
	positions = [];
	var sequence = [];
	for(var k = 0; k < numberOfSingleSeries; k++)
		sequence = sequence.concat(getSymbols(checkSymbols));
	sequence.push(" ");
	console.log(sequence);
	return sequence;
}

/** 
* Draws single symbol by drawing it from pseudo-random number.
* @global
* @method getSymbols
* @param {object} checkSymbols symbols that are concated in each iteration to sequence
* @return {array} symbols in single sequence
*/
function getSymbols(checkSymbols){
	var symbols = constSymbols.slice();
	var testSymbols = [];
	var randomIndex;
	var sampleSymbol;

	for (var i = 0; i < testSymbolsNumber; i++){
		randomIndex = Math.floor(Math.random() * symbols.length);
		sampleSymbol = symbols.splice(randomIndex, 1)[0];
		testSymbols.push(sampleSymbol)
	}

	for(var j = 0; j < checkSymbols; j++){
		if(hasSymbolOccured()){
			randomIndex = Math.floor(Math.random() * testSymbolsNumber);
			positions.push(randomIndex);
			testSymbols.push(testSymbols[randomIndex]);
		}
		else{
			randomIndex = Math.floor(Math.random() * symbols.length);
			positions.push(testSymbolsNumber);
			testSymbols.push(symbols[randomIndex]);
		}
	}
	testSymbols.unshift(" ");
	return testSymbols;
}

/** 
* Decides whether test symbol is from appeared symbols or other
* @global
* @method hasSymbolOccured
* @return {Boolean} val
*/
function hasSymbolOccured() {
	if(Math.random() > 0.333)
		return true;
	else
		return false;
}

