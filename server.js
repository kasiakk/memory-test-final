var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var fs = require("fs");
var fileName = "default";

app.use(express.static('front'));
app.use(bodyParser.urlencoded({extended: true}));

/** 
* Saves request to file named with index number and sex
* @global
* @method anonymous-function
*/
app.post('/sternbergTest', function (req, res) {
   console.log(req.body);
   var fileName = req.body.indexNumber + req.body.sex + ".json";
   var data = JSON.stringify(req.body.results).replace(/\\/g, "");
   data = data.substring(2, data.length - 5);
   fs.writeFile(fileName, "[" + data + "]", function (err) {
	 console.log(err);
    res.send("OK")
 });   
})

/** 
* Enables user to get base html file with alias 
* @global
* @method anonymous-function
*/

app.get('/', function(req, res) {  
   res.render('front/index.html');
});

/** 
* Starts server.
* @global
* @method anonymous-function
*/
var server = app.listen(50900, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})
